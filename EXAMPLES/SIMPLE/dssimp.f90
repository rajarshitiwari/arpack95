PROGRAM DSSIMP
  !
  !     This example program is intended to illustrate the
  !     simplest case of using ARPACK in considerable detail.
  !     This code may be used to understand basic usage of ARPACK
  !     and as a template for creating an interface to ARPACK.
  !
  !     This code shows how to use ARPACK to find a few eigenvalues
  !     (lambda) and corresponding eigenvectors (x) for the standard
  !     eigenvalue problem:
  !
  !                        A*x = lambda*x
  !
  !     where A is an n by n real symmetric matrix.
  !
  !     The main points illustrated here are
  !
  !        1) How to declare sufficient memory to find NEV
  !           eigenvalues of largest magnitude.  Other options
  !           are available.
  !
  !        2) Illustration of the reverse communication interface
  !           needed to utilize the top level ARPACK routine DSAUPD
  !           that computes the quantities needed to construct
  !           the desired eigenvalues and eigenvectors(if requested).
  !
  !        3) How to extract the desired eigenvalues and eigenvectors
  !           using the ARPACK routine DSEUPD.
  !
  !     The only thing that must be supplied in order to use this
  !     routine on your problem is to change the array dimensions
  !     appropriately, to specify WHICH eigenvalues you want to compute
  !     and to supply a matrix-vector product
  !
  !                         w <-  Av
  !
  !     in place of the call to AV( ) below.
  !
  !     Once usage of this routine is understood, you may wish to explore
  !     the other available options to improve convergence, to solve gener
  !     problems, etc.  Look at the file ex-sym.doc in DOCUMENTS directory
  !     This codes implements
  !
  !\Example-1
  !     ... Suppose we want to solve A*x = lambda*x in regular mode,
  !         where A is derived from the central difference discretization
  !         of the 2-dimensional Laplacian on the unit square with
  !         zero Dirichlet boundary condition.
  !     ... OP = A  and  B = I.
  !     ... Assume "call av (n,x,y)" computes y = A*x
  !     ... Use mode 1 of DSAUPD.
  !
  !\BeginLib
  !
  !\Routines called:
  !     dsaupd  ARPACK reverse communication interface routine.
  !     dseupd  ARPACK routine that returns Ritz values and (optionally)
  !             Ritz vectors.
  !     dnrm2   Level 1 BLAS that computes the norm of a vector.
  !     daxpy   Level 1 BLAS that computes y <- alpha*x+y.
  !
  !\Author
  !     Richard Lehoucq
  !     Danny Sorensen
  !     Chao Yang
  !     Dept. of Computational &
  !     Applied Mathematics
  !     Rice University
  !     Houston, Texas
  !
  !\SCCS Information: @(#)
  ! FILE: ssimp.F   SID: 2.6   DATE OF SID: 10/17/00   RELEASE: 2
  !
  !\Remarks
  !     1. None
  !
  !\EndLib
  !
  !-----------------------------------------------------------------------
  !
  !     %------------------------------------------------------%
  !     | Storage Declarations:                                |
  !     |                                                      |
  !     | The maximum dimensions for all arrays are            |
  !     | set here to accommodate a problem size of            |
  !     | N .le. MAXN                                          |
  !     |                                                      |
  !     | NEV is the number of eigenvalues requested.          |
  !     |     See specifications for ARPACK usage below.       |
  !     |                                                      |
  !     | NCV is the largest number of basis vectors that will |
  !     |     be used in the Implicitly Restarted Arnoldi      |
  !     |     Process.  Work per major iteration is            |
  !     |     proportional to N*NCV*NCV.                       |
  !     |                                                      |
  !     | You must set:                                        |
  !     |                                                      |
  !     | MAXN:   Maximum dimension of the A allowed.          |
  !     | MAXNEV: Maximum NEV allowed.                         |
  !     | MAXNCV: Maximum NCV allowed.                         |
  !     %------------------------------------------------------%
  !

  INTEGER ::  MAXN, MAXNEV, MAXNCV, LDV
  PARAMETER (MAXN=256, MAXNEV=10, MAXNCV=25, LDV=MAXN )

  !
  !     %--------------%
  !     | Local Arrays |
  !     %--------------%
  !
  REAL(8) :: V(LDV,MAXNCV), WORKL(MAXNCV*(MAXNCV+8)), WORKD(3*MAXN), D(MAXNCV,2), RESID(MAXN), AX(MAXN)
  LOGICAL :: SELECT(MAXNCV)
  INTEGER :: IPARAM(11), IPNTR(11)

  !
  !     %---------------%
  !     | Local Scalars |
  !     %---------------%
  !
  CHARACTER :: BMAT*1, WHICH*2
  INTEGER   :: IDO, N, NEV, NCV, LWORKL, INFO, IERR, J, NX, ISHFTS, MAXITR, MODE1, NCONV
  LOGICAL   :: RVEC
  REAL(8)   :: TOL, SIGMA

  !
  !     %------------%
  !     | Parameters |
  !     %------------%
  !
  REAL(8), PARAMETER :: ZERO = 0.0D+0

  !
  !     %-----------------------------%
  !     | BLAS & LAPACK routines used |
  !     %-----------------------------%
  !
  REAL(8) :: DNRM2
  EXTERNAL DNRM2, DAXPY

  !
  !     %--------------------%
  !     | Intrinsic function |
  !     %--------------------%
  !
  INTRINSIC ABS
  !
  !     %-----------------------%
  !     | Executable Statements |
  !     %-----------------------%
  !

  !     %-------------------------------------------------%
  !     | The following include statement and assignments |
  !     | initiate trace output from the internal         |
  !     | actions of ARPACK.  See debug.doc in the        |
  !     | DOCUMENTS directory for usage.  Initially, the  |
  !     | most useful information will be a breakdown of  |
  !     | time spent in the various stages of computation |
  !     | given by setting msaupd = 1.                    |
  !     %-------------------------------------------------%
  !

  !
  !\SCCS Information: @(#) 
  ! FILE: debug.h   SID: 2.3   DATE OF SID: 11/16/95   RELEASE: 2 
  !
  !     %---------------------------------%
  !     | See debug.doc for documentation |
  !     %---------------------------------%
  INTEGER :: logfil, ndigit, mgetv0, msaupd, msaup2, msaitr, mseigt, msapps, msgets, mseupd, mnaupd, mnaup2,&
       & mnaitr, mneigh, mnapps, mngets, mneupd, mcaupd, mcaup2, mcaitr, mceigh, mcapps, mcgets, mceupd
  COMMON /DEBUG/ logfil, ndigit, mgetv0, msaupd, msaup2, msaitr, mseigt, msapps, msgets, mseupd, mnaupd, mnaup2,&
       & mnaitr, mneigh, mnapps, mngets, mneupd, mcaupd, mcaup2, mcaitr, mceigh, mcapps, mcgets, mceupd


  NDIGIT = -3
  LOGFIL = 6
  MSGETS = 0
  MSAITR = 0
  MSAPPS = 0
  MSAUPD = 1
  MSAUP2 = 0
  MSEIGT = 0
  MSEUPD = 0
  !
  !     %-------------------------------------------------%
  !     | The following sets dimensions for this problem. |
  !     %-------------------------------------------------%
  !
  NX = 10
  N = NX*NX
  !

  !     %-----------------------------------------------%
  !     |                                               |
  !     | Specifications for ARPACK usage are set       |
  !     | below:                                        |
  !     |                                               |
  !     |    1) NEV = 4  asks for 4 eigenvalues to be   |
  !     |       computed.                               |
  !     |                                               |
  !     |    2) NCV = 20 sets the length of the Arnoldi |
  !     |       factorization                           |
  !     |                                               |
  !     |    3) This is a standard problem              |
  !     |         (indicated by bmat  = 'I')            |
  !     |                                               |
  !     |    4) Ask for the NEV eigenvalues of          |
  !     |       largest magnitude                       |
  !     |         (indicated by which = 'LM')           |
  !     |       See documentation in DSAUPD for the     |
  !     |       other options SM, LA, SA, LI, SI.       |
  !     |                                               |
  !     | Note: NEV and NCV must satisfy the following  |
  !     | conditions:                                   |
  !     |              NEV <= MAXNEV                    |
  !     |          NEV + 1 <= NCV <= MAXNCV             |
  !     %-----------------------------------------------%
  !
  NEV   = 4
  NCV   = 20
  BMAT  = 'I'
  WHICH = 'LM'
  !

  IF ( N .GT. MAXN ) THEN
     PRINT *, ' ERROR with _SSIMP: N is greater than MAXN '
     GO TO 9000
  ELSE IF ( NEV .GT. MAXNEV ) THEN
     PRINT *, ' ERROR with _SSIMP: NEV is greater than MAXNEV '
     GO TO 9000
  ELSE IF ( NCV .GT. MAXNCV ) THEN
     PRINT *, ' ERROR with _SSIMP: NCV is greater than MAXNCV '
     GO TO 9000
  END IF

  !
  !     %-----------------------------------------------------%
  !     |                                                     |
  !     | Specification of stopping rules and initial         |
  !     | conditions before calling DSAUPD                    |
  !     |                                                     |
  !     | TOL  determines the stopping criterion.             |
  !     |                                                     |
  !     |      Expect                                         |
  !     |           abs(lambdaC - lambdaT) < TOL*abs(lambdaC) |
  !     |               computed   true                       |
  !     |                                                     |
  !     |      If TOL .le. 0,  then TOL <- macheps            |
  !     |           (machine precision) is used.              |
  !     |                                                     |
  !     | IDO  is the REVERSE COMMUNICATION parameter         |
  !     |      used to specify actions to be taken on return  |
  !     |      from DSAUPD. (See usage below.)                |
  !     |                                                     |
  !     |      It MUST initially be set to 0 before the first |
  !     |      call to DSAUPD.                                |
  !     |                                                     |
  !     | INFO on entry specifies starting vector information |
  !     |      and on return indicates error codes            |
  !     |                                                     |
  !     |      Initially, setting INFO=0 indicates that a     |
  !     |      random starting vector is requested to         |
  !     |      start the ARNOLDI iteration.  Setting INFO to  |
  !     |      a nonzero value on the initial call is used    |
  !     |      if you want to specify your own starting       |
  !     |      vector (This vector must be placed in RESID.)  |
  !     |                                                     |
  !     | The work array WORKL is used in DSAUPD as           |
  !     | workspace.  Its dimension LWORKL is set as          |
  !     | illustrated below.                                  |
  !     |                                                     |
  !     %-----------------------------------------------------%
  !
  LWORKL = NCV*(NCV+8)
  TOL = ZERO
  INFO = 0
  IDO = 0
  !

  !     %---------------------------------------------------%
  !     | Specification of Algorithm Mode:                  |
  !     |                                                   |
  !     | This program uses the exact shift strategy        |
  !     | (indicated by setting PARAM(1) = 1).              |
  !     | IPARAM(3) specifies the maximum number of Arnoldi |
  !     | iterations allowed.  Mode 1 of DSAUPD is used     |
  !     | (IPARAM(7) = 1). All these options can be changed |
  !     | by the user. For details see the documentation in |
  !     | DSAUPD.                                           |
  !     %---------------------------------------------------%
  !
  ISHFTS = 1
  MAXITR = 300
  MODE1 = 1
  !
  IPARAM(1) = ISHFTS
  !
  IPARAM(3) = MAXITR
  !
  IPARAM(7) = MODE1
  !

  !     %------------------------------------------------%
  !     | M A I N   L O O P (Reverse communication loop) |
  !     %------------------------------------------------%
  !
  MAIN_LOOP_REVERSE_COMM: DO
     !
     !        %---------------------------------------------%
     !        | Repeatedly call the routine DSAUPD and take |
     !        | actions indicated by parameter IDO until    |
     !        | either convergence is indicated or maxitr   |
     !        | has been exceeded.                          |
     !        %---------------------------------------------%
     !
     CALL DSAUPD ( IDO, BMAT, N, WHICH, NEV, TOL, RESID, ncv, v, ldv, iparam, ipntr, workd, workl, lworkl, info )
     !
     IF (IDO .EQ. -1 .OR. IDO .EQ. 1) THEN
        !
        !           %--------------------------------------%
        !           | Perform matrix vector multiplication |
        !           |              y <--- OP*x             |
        !           | The user should supply his/her own   |
        !           | matrix vector multiplication routine |
        !           | here that takes workd(ipntr(1)) as   |
        !           | the input, and return the result to  |
        !           | workd(ipntr(2)).                     |
        !           %--------------------------------------%
        !
        CALL AV (NX, WORKD(IPNTR(1)), WORKD(IPNTR(2)))
        !
        !           %-----------------------------------------%
        !           | L O O P   B A C K to call DSAUPD again. |
        !           %-----------------------------------------%
        !
        !
     ELSE
        EXIT
     END IF
  END DO MAIN_LOOP_REVERSE_COMM
  !

  !     %----------------------------------------%
  !     | Either we have convergence or there is |
  !     | an error.                              |
  !     %----------------------------------------%
  !
  IF ( INFO .LT. 0 ) THEN
     !
     !        %--------------------------%
     !        | Error message. Check the |
     !        | documentation in DSAUPD. |
     !        %--------------------------%
     !
     PRINT *, ' '
     PRINT *, ' Error with _saupd, info = ', INFO
     PRINT *, ' Check documentation in _saupd '
     PRINT *, ' '
     !

  ELSE
     !
     !        %-------------------------------------------%
     !        | No fatal errors occurred.                 |
     !        | Post-Process using DSEUPD.                |
     !        |                                           |
     !        | Computed eigenvalues may be extracted.    |
     !        |                                           |
     !        | Eigenvectors may be also computed now if  |
     !        | desired.  (indicated by rvec = .true.)    |
     !        |                                           |
     !        | The routine DSEUPD now called to do this  |
     !        | post processing (Other modes may require  |
     !        | more complicated post processing than     |
     !        | mode1.)                                   |
     !        |                                           |
     !        %-------------------------------------------%
     !
     RVEC = .TRUE.
     !
     CALL DSEUPD ( RVEC, 'All', SELECT, D, V, LDV, SIGMA, BMAT, N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM, IPNTR, WORKD, WORKL, LWORKL, IERR )
     !
     !         %----------------------------------------------%
     !         | Eigenvalues are returned in the first column |
     !         | of the two dimensional array D and the       |
     !         | corresponding eigenvectors are returned in   |
     !         | the first NCONV (=IPARAM(5)) columns of the  |
     !         | two dimensional array V if requested.        |
     !         | Otherwise, an orthogonal basis for the       |
     !         | invariant subspace corresponding to the      |
     !         | eigenvalues in D is returned in V.           |
     !         %----------------------------------------------%
     !

     IF ( IERR .NE. 0) THEN
        !
        !            %------------------------------------%
        !            | Error condition:                   |
        !            | Check the documentation of DSEUPD. |
        !            %------------------------------------%
        !
        PRINT *, ' '
        PRINT *, ' Error with _seupd, info = ', ierr
        PRINT *, ' Check the documentation of _seupd. '
        PRINT *, ' '
        !
     ELSE
        !

        NCONV =  IPARAM(5)
        DO J=1, NCONV
           !
           !               %---------------------------%
           !               | Compute the residual norm |
           !               |                           |
           !               |   ||  A*x - lambda*x ||   |
           !               |                           |
           !               | for the NCONV accurately  |
           !               | computed eigenvalues and  |
           !               | eigenvectors.  (iparam(5) |
           !               | indicates how many are    |
           !               | accurate to the requested |
           !               | tolerance)                |
           !               %---------------------------%
           !
           CALL AV(NX, V(1,J), AX)
           CALL DAXPY(N, -D(J,1), V(1,J), 1, AX, 1)
           D(J,2) = DNRM2(N, AX, 1)
           D(J,2) = D(J,2) / ABS(D(J,1))
           !
        END DO
        !
        !            %-----------------------------%
        !            | Display computed residuals. |
        !            %-----------------------------%
        !
        CALL DMOUT(6, NCONV, 2, D, MAXNCV, -6, 'Ritz values and relative residuals')
     END IF
     !

     !         %-------------------------------------------%
     !         | Print additional convergence information. |
     !         %-------------------------------------------%
     !
     IF ( INFO .EQ. 1) THEN
        PRINT *, ' '
        PRINT *, ' Maximum number of iterations reached.'
        PRINT *, ' '
     ELSE IF ( INFO .EQ. 3) THEN
        PRINT *, ' '
        PRINT *, ' No shifts could be applied during implicit', ' Arnoldi update, try increasing NCV.'
        PRINT *, ' '
     END IF
     !

     PRINT *, ' '
     PRINT *, ' _SSIMP '
     PRINT *, ' ====== '
     PRINT *, ' '
     PRINT *, ' Size of the matrix is ', N
     PRINT *, ' The number of Ritz values requested is ', NEV
     PRINT *, ' The number of Arnoldi vectors generated', ' (NCV) is ', NCV
     PRINT *, ' What portion of the spectrum: ', WHICH
     PRINT *, ' The number of converged Ritz values is ', NCONV
     PRINT *, ' The number of Implicit Arnoldi update',  ' iterations taken is ', IPARAM(3)
     PRINT *, ' The number of OP*x is ', IPARAM(9)
     PRINT *, ' The convergence criterion is ', TOL
     PRINT *, ' '
     !
  END IF
  !
  !     %---------------------------%
  !     | Done with program dssimp. |
  !     %---------------------------%
  !
9000 continue
  !
END PROGRAM DSSIMP
!

! ------------------------------------------------------------------
!     matrix vector subroutine
!
!     The matrix used is the 2 dimensional discrete Laplacian on unit
!     square with zero Dirichlet boundary condition.
!
!     Computes w <--- OP*v, where OP is the nx*nx by nx*nx block
!     tridiagonal matrix
!
!                  | T -I          |
!                  |-I  T -I       |
!             OP = |   -I  T       |
!                  |        ...  -I|
!                  |           -I T|
!
!     The subroutine TV is called to computed y<---T*x.
!
SUBROUTINE AV (NX, V, W)
  INTEGER :: NX, J, LO, N2
  REAL(8) :: V(NX*NX), W(NX*NX), H2
  REAL(8), PARAMETER ::  ONE = 1.0D+0
  !
  CALL TV(NX,V(1),W(1))
  CALL DAXPY(NX, -ONE, V(NX+1), 1, W(1), 1)
  !
  DO J = 2, NX-1
     LO = (J-1)*NX
     CALL TV(NX, V(LO+1), W(LO+1))
     CALL DAXPY(NX, -ONE, V(LO-NX+1), 1, W(LO+1), 1)
     CALL DAXPY(NX, -ONE, V(LO+NX+1), 1, W(LO+1), 1)
  END DO
  !

  LO = (NX-1)*NX
  CALL TV(NX, V(LO+1), W(LO+1))
  CALL DAXPY(NX, -ONE, V(LO-NX+1), 1, W(LO+1), 1)
  !

  !     Scale the vector w by (1/h^2), where h is the mesh size
  !
  N2 = NX*NX
  H2 = ONE / DBLE((NX+1)*(NX+1))
  CALL DSCAL(N2, ONE/H2, W, 1)
  RETURN
END SUBROUTINE AV
!

!-------------------------------------------------------------------
SUBROUTINE TV (NX, X, Y)
  !
  INTEGER :: NX, J
  REAL(8) :: X(NX), Y(NX), DD, DL, DU
  !
  REAL(8), PARAMETER :: ONE = 1.0D+0, FOUR = 4.0D+0
  !
  !     Compute the matrix vector multiplication y<---T*x
  !     where T is a nx by nx tridiagonal matrix with DD on the
  !     diagonal, DL on the subdiagonal, and DU on the superdiagonal.
  !
  !

  DD  = FOUR
  DL  = -ONE
  DU  = -ONE
  !
  Y(1) =  DD*X(1) + DU*X(2)
  DO J = 2, NX-1
     Y(J) = DL*X(J-1) + DD*X(J) + DU*X(J+1)
  END DO
  Y(NX) =  DL*X(NX-1) + DD*X(NX)
  RETURN
END SUBROUTINE TV
